
const { readInput, groupData, powerPuffData, removeId, sortBasedOnCompany, swap, employeeBirthday } = require("./main");
let input = "./data.json";



readInput(input).then((result) => {
    return groupData(result);
}).then((result) => {
    return powerPuffData(result);
}).then((result) => {
    return removeId(result);
}).then((result) => {
    return sortBasedOnCompany(result);
}).then((result) => {
    return swap(result);
}).then((result) => {
    return employeeBirthday(result)
}).catch((error) => {
    console.error("Error occured " + error);
})

