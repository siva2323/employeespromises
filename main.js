const fs = require("fs");
let input = "./data.json";



function readInput(input) {
    return new Promise((resolve, reject) => {
        fs.readFile("./data.json", "utf-8", (error, inputData) => {
            if (error) {
                reject(error)
            } else {
                let ids = [2, 13, 23];
                try {
                    inputData = JSON.parse(inputData);
                } catch (error) {
                    reject(error)
                }

                let answer = inputData["employees"].filter((element) => ids.includes(element.id))
                try {
                    answer = JSON.stringify(answer);
                } catch (error) {
                    reject(error);
                }
                fs.writeFile("./answer/answerRetrieveData.json", answer, "utf-8", (error) => {
                    if (error) {
                        reject(error)
                    } else {
                        resolve(inputData)
                    }
                })
            }
        })
    }
    )
}


function groupData(inputData) {
    return new Promise((resolve, reject) => {
        let answer = inputData["employees"].reduce((accumulator, element) => {
            if (accumulator[element.company]) {
                accumulator[element["company"]].push(element)
            }
            return accumulator;

        }, { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": [] })

        try {
            answer = JSON.stringify(answer);
        } catch (error) {
            reject(error)
        }

        fs.writeFile("./answer/answerGroupData.json", answer, "utf-8", (error) => {
            if (error) {
                reject(error)
            } else {
                resolve(inputData);
            }
        })
    })
}



function powerPuffData(inputData) {
    return new Promise((resolve, reject) => {
        let answer = inputData["employees"].filter((element) => "Powerpuff Brigade".includes(element.company))

        try {
            answer = JSON.stringify(answer);
        } catch (error) {
            reject(error)
        }

        fs.writeFile("./answer/answerPowerPuffData.json", answer, "utf-8", (error) => {
            if (error) {
                reject(error);
            } else {
                resolve(inputData);
            }
        })
    })
}


function removeId(inputData) {
    return new Promise((resolve, reject) => {
        let answer = inputData["employees"].filter((element) => (element.id !== 2))

        try {
            answer = JSON.stringify(answer);
        } catch (error) {
            reject(error)
        }

        fs.writeFile("./answer/answerRemoveId.json", answer, "utf-8", (error) => {
            if (error) {
                reject(error)
            } else {
                resolve(inputData)
            }
        })
    })
}


function sortBasedOnCompany(inputData) {
    return new Promise((resolve, reject) => {
        let answer = inputData.employees.sort((item1, item2) => {
            if (item1.company < item2.company) {
                return -1;
            }
            else if (item1.company > item2.company) {
                return 1;
            }
            else {
                if (item1.id < item2.id) {
                    return -1;
                }
                else if (item1.id > item2.id) {
                    return 1;
                }
                else {
                    return 0;
                }
            }
        })
        try {
            answer = JSON.stringify(answer);
        } catch (error) {
            reject(error)
        }

        fs.writeFile("./answer/answerSortBasedOnCampany.json", answer, "utf-8", (error) => {
            if (error) {
                reject(error)
            } else {
                resolve(inputData)
            }
        })
    })
}


function swap(inputData) {
    return new Promise((resolve, reject) => {
        let ninetyTwo = inputData.employees.filter((item) => {
            return item.id === 92;
        })
        let ninetyThree = inputData.employees.filter((item) => {
            return item.id === 93;
        })
        let answer = inputData.employees.map((item) => {
            if (item.id === 92) {
                return ninetyThree;
            }
            else if (item.id === 93) {
                return ninetyTwo;
            }
            return item;
        })
        try {
            answer = JSON.stringify(answer);
        } catch (error) {
            reject(error)
        }

        fs.writeFile("./answer/answerSwap.json", answer, "utf-8", (error) => {
            if (error) {
                reject(error)
            } else {
                resolve(inputData);
            }
        })
    })
}

function employeeBirthday(inputData) {
    return new Promise((resolve, reject) => {
        let answer = inputData.employees.map((item) => {
            if ((item.id) % 2 == 0) {
                item["birthday"] = new Date().getDate();
            }
            return item;
        })

        try {
            answer = JSON.stringify(answer);
        } catch (error) {
            reject(error)
        }


        fs.writeFile("./answer/answerEmployeeBirthday.json", answer, "utf-8", (error) => {
            if (error) {
                reject(error)
            } else {
                resolve(inputData)
            }
        })
    })
}




module.exports.readInput = readInput;
module.exports.groupData = groupData;
module.exports.powerPuffData = powerPuffData;
module.exports.removeId = removeId;
module.exports.sortBasedOnCompany = sortBasedOnCompany;
module.exports.swap = swap;
module.exports.employeeBirthday = employeeBirthday;

